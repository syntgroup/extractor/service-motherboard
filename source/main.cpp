#include "main.h"

#include "service_srvint_window.h"
#include "transfer/executor.h"
#include "transfer/srvintsession.h"
#include <KAboutData>
#include <QApplication>
#include <QStandardPaths>
#include <QsLog/QsLog.h>

namespace {
constexpr QsLogging::MaxSizeBytes max_bytes{ static_cast<qint64>(10e7) };
constexpr QsLogging::MaxOldLogCount max_old_logs{ 1 };
constexpr auto rotate_logs{ QsLogging::EnableLogRotation };
constexpr auto logs_level{ QsLogging::TraceLevel };

constexpr auto app_name{ "service-srvint" };
} // namespace

auto
main(int argc, char* argv[]) -> int
{
  QApplication application{ argc, argv };

  // set up QApplication data
  QApplication::setWindowIcon(QIcon{ ":/logo/icons8-console-96.png" });
  QApplication::setOrganizationName(Main::CompanyName());
  QApplication::setOrganizationDomain(Main::CompanyDomain());
  QApplication::setApplicationName(app_name);
  QApplication::setApplicationDisplayName(QApplication::applicationName());

  // set up kde application metadata
  KAboutData about_data{
    app_name,
    app_name,
    VersionInfo::ProjectVersion(),
    QStringLiteral("Service application to test and configure motherboard "
                   "found in Colibri device"),
    KAboutLicense::LGPL_V2_1,
    QStringLiteral("(c) 2023"),
    QLatin1String{},
    QStringLiteral(
      "https://gitlab.com/syntgroup/extractor/service-motherboard"),
    QStringLiteral("contact-project+syntgroup-extractor-service-motherboard-"
                   "42722654-issue-@incoming.gitlab.com")
  };
  about_data.setProgramLogo(QIcon{ ":/logo/icons8-console-96.png" });

  about_data.addAuthor(QStringLiteral("Tim May"),
                       QStringLiteral("Developer"),
                       QStringLiteral("t.mayzenberg@lambda-it.ru"),
                       QStringLiteral("https://gitlab.com/murych"));

  about_data.addComponent(
    QStringLiteral("Colibri core"),
    QStringLiteral(""),
    QStringLiteral("0.6"),
    QStringLiteral("https://gitlab.com/syntgroup/extractor/extractor-panel"),
    KAboutLicense::LGPL_V2_1);

  KAboutData::setApplicationData(about_data);

  // set up logger
  auto& logger{ QsLogging::Logger::instance() };
  logger.setLoggingLevel(logs_level);

  const auto s_log_path{ QStandardPaths::writableLocation(
                           QStandardPaths::AppDataLocation) +
                         QStringLiteral("/") + QApplication::applicationName() +
                         QStringLiteral(".log") };
  logger.addDestination(QsLogging::DestinationFactory::MakeFileDestination(
    s_log_path, rotate_logs, max_bytes, max_old_logs));
  logger.addDestination(
    QsLogging::DestinationFactory::MakeDebugOutputDestination());

  QLOG_INFO().noquote() << QApplication::applicationName()
                        << QApplication::applicationVersion() << "STARTING";

  //----------------------------------------------------------------------------

  // create backend entities
  transfer::SrvIntSession session;
  transfer::Executor executor{ session };

  // create main window
  ServiceSrvIntWindow window{ session, executor };
  window.show();

  return QApplication::exec();
}
