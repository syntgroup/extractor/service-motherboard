#ifndef SERVICE_APP_SRVINT_MAINWINDOW_H
#define SERVICE_APP_SRVINT_MAINWINDOW_H

#include <QMainWindow>
#include <gsl/gsl-lite.hpp>

class KLed;
class KPageWidget;
class KPageWidgetItem;

namespace Ui {
class ServiceSrvIntWindow;
} // namespace Ui

namespace transfer {
class Executor;
class SrvIntSession;
} // namespace transfer

class ServiceSrvIntWindow : public QMainWindow
{
  Q_OBJECT
  Q_DISABLE_COPY_MOVE(ServiceSrvIntWindow)

public:
  ServiceSrvIntWindow(transfer::SrvIntSession& session,
                      transfer::Executor& executor,
                      QWidget* parent = nullptr);
  ~ServiceSrvIntWindow() override;

private:
  std::unique_ptr<Ui::ServiceSrvIntWindow> m_ui{ nullptr };
  std::unique_ptr<KLed> m_led{ nullptr };

  const std::vector<gsl::owner<KPageWidgetItem*>> m_persistentPages;
  const std::vector<gsl::owner<KPageWidgetItem*>> m_conditionalPages;

  void setupConnections();

private slots:
  void switchWidgetsEnabled(bool state);
};

#endif  // SERVICE_APP_SRVINT_MAINWINDOW_H
