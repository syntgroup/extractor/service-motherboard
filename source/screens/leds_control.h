#ifndef SCREENS_LEDS_CONTROL_H
#define SCREENS_LEDS_CONTROL_H

#include "motherboard/device/motherboard_values.h"
#include <QWidget>
#include <vector>

namespace transfer {
class Executor;
}

namespace device {
class Motherboard;
class RgbLeds;
}

namespace screen {

namespace Ui {
class LedsControl;
} // namespace Ui

class LedsControl : public QWidget
{
  Q_OBJECT

public:
  explicit LedsControl(transfer::Executor& executor, QWidget* parent = nullptr);
  ~LedsControl() override;

private:
  std::reference_wrapper<transfer::Executor> m_executor;
  std::unique_ptr<Ui::LedsControl> m_ui{ nullptr };
  std::shared_ptr<device::RgbLeds> m_leds;
  std::vector<device::Color> colors;

  // SerialAccessible interface
protected:
  void setupConnections();

private slots:
  void changeOneColor(int idx, QColor color);
  void changeAllColors(QColor color);
};

} // namespace screens
#endif // SCREENS_LEDS_CONTROL_H
