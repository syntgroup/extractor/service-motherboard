#ifndef SCREEN_POWER_CONTROL_H
#define SCREEN_POWER_CONTROL_H

#include <QWidget>

class QPushButton;

namespace transfer {
class Executor;
} // namespace transfer

namespace device {
class LedLamp;
class UvLamp;
} // namespace device

namespace screen {

namespace Ui {
class PowerControl;
} // namespace Ui

class PowerControl final : public QWidget
{
  Q_OBJECT
  Q_DISABLE_COPY_MOVE(PowerControl);

public:
  explicit PowerControl(transfer::Executor& executor,
                        QWidget* parent = nullptr);
  ~PowerControl() override;

private:
  std::reference_wrapper<transfer::Executor> m_executor;

  std::unique_ptr<Ui::PowerControl> m_ui{ nullptr };
  std::shared_ptr<device::LedLamp> m_lampLed;
  std::shared_ptr<device::UvLamp> m_lampUv;

  static void toggleButton(QPushButton& button, bool state);

private slots:
  void setupConnections();
};

} // namespace screen

#endif // SCREEN_POWER_CONTROL_H
