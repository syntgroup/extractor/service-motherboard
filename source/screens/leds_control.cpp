#include "leds_control.h"
#include "ui_leds_control.h"

#include "motherboard/cmd/rgb_led_set_operation.h"
#include "motherboard/device/motherboard_values.h"
#include "motherboard/device/rgb_led.h"
#include "motherboard/device/rgb_leds.h"
#include "transfer/executor.h"
#include "utils/color_converter.h"
#include <KLed>
#include <QColor>
#include <QHBoxLayout>
#include <QMessageBox>
#include <QTimer>
#include <QsLog/QsLog.h>

namespace screen {

LedsControl::LedsControl(transfer::Executor& executor, QWidget* parent)
  : QWidget{ parent }
  , m_executor{ executor }
  , m_ui{ std::make_unique<Ui::LedsControl>() }
  , m_leds{ std::make_shared<device::RgbLeds>() }
{
  m_ui->setupUi(this);

  setupConnections();
}

LedsControl::~LedsControl() = default;

void
LedsControl::setupConnections()
{
  auto* layout{ new QHBoxLayout{ this } };
  m_ui->leds->setLayout(layout);
  //  for (int i = 0; i < 5; ++i) {
  //    auto* led{ m_motherboard->rgbLeds()->led(i) };
  //    auto* button{ new KColorButton{
  //      utils::ColorConverter::mapDeviceColorToQColor(led->color()), this } };

  //    button->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);
  //    disconnect(button, &KColorButton::clicked, nullptr, nullptr);
  //    connect(button, &KColorButton::clicked, this, [this, button, i] {
  //      dialogs::ColorSelect select;

  //      auto result = select.exec();
  //      if (result != QDialog::Accepted) {
  //        return;
  //      }

  //      auto color{ select.result() };
  //      changeOneColor(i, color);
  //    });

  //    connect(
  //      led, &device::RgbLed::colorChanged, led, [button](device::Color color)
  //      {
  //        auto new_color{ utils::ColorConverter::mapDeviceColorToQColor(color)
  //        }; button->setColor(new_color);
  //      });

  //    colors.emplace(i, led->color());
  //    layout->addWidget(button);
  //  }

  // ---------------------------------------------------------------------------

  const std::array buttons{ std::make_pair(m_ui->all_off, Qt::transparent),
                            std::make_pair(m_ui->all_cyan, Qt::cyan),
                            std::make_pair(m_ui->all_magenta, Qt::magenta),
                            std::make_pair(m_ui->all_red, Qt::red),
                            std::make_pair(m_ui->all_blue, Qt::blue),
                            std::make_pair(m_ui->all_green, Qt::green),
                            std::make_pair(m_ui->all_yellow, Qt::yellow),
                            std::make_pair(m_ui->all_white, Qt::white) };
  std::for_each(buttons.begin(), buttons.end(), [this](auto&& button) {
    connect(button.first, &QPushButton::clicked, this, [this, button] {
      changeAllColors(button.second);
    });
  });
}

void
LedsControl::changeOneColor(int idx, QColor color)
{
  QLOG_DEBUG() << this << "plate" << idx << "request change color to" << color;

  //  colors.insert_or_assign(idx,
  //                          utils::ColorConverter::mapQColorToDeviceColor(color));
  colors[idx] = utils::ColorConverter::mapQColorToDeviceColor(color);
  m_executor.get().execute<cmd::motherboard::RgbLedSetOperation>(m_leds,
                                                                 colors);
}

void
LedsControl::changeAllColors(QColor color)
{
  QLOG_DEBUG() << this << "request change ALL colors to" << color;

  for (auto& item : colors) {
    item = utils::ColorConverter::mapQColorToDeviceColor(color);
  }
  m_executor.get().execute<cmd::motherboard::RgbLedSetOperation>(m_leds,
                                                                 colors);
}

} // namespace screens
