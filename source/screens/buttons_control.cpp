#include "buttons_control.h"
#include "ui_buttons_control.h"

#include "motherboard/cmd/buttons_request_door.h"
#include "motherboard/cmd/buttons_request_power_button.h"
#include "motherboard/cmd/diag_opt_state_operation.h"
#include "motherboard/cmd/power_button_clear_trigger.h"
#include "motherboard/cmd/power_get_buttons_operation.h"
#include "motherboard/device/button.h"
#include "motherboard/device/powers.h"
#include "transfer/executor.h"
#include <QTimer>
#include <QsLog/QsLog.h>

namespace screen {

ButtonsControl::ButtonsControl(transfer::Executor& executor, QWidget* parent)
  : QWidget{ parent }
  , m_executor{ executor }
  , m_ui{ std::make_unique<Ui::ButtonsControl>() }
  , m_pollTimer{ std::make_unique<QTimer>() }
  , m_doorButton{ std::make_shared<device::Button>() }
  , m_powerButton{ std::make_shared<device::Button>() }
  , m_diagSensor{ std::make_shared<device::Button>() }
{
  m_ui->setupUi(this);
  m_pollTimer->setSingleShot(false);

  connect(m_ui->pollAutoSpinBox,
          QOverload<int>::of(&QSpinBox::valueChanged),
          m_pollTimer.get(),
          QOverload<int>::of(&QTimer::setInterval));

  connect(
    m_pollTimer.get(), &QTimer::timeout, this, &ButtonsControl::pollRoutine);

  connect(m_ui->pollManualButton,
          &QToolButton::clicked,
          this,
          &ButtonsControl::pollRoutine);

  connect(
    m_ui->pollAutoButton, &QToolButton::toggled, this, [this](bool checked) {
      m_ui->pollManualButton->setEnabled(!checked);
      checked ? m_pollTimer->start() : m_pollTimer->stop();
    });

  setupConnections();
}

ButtonsControl::~ButtonsControl() = default;

void
ButtonsControl::setupConnections()
{
  connect(
    m_doorButton.get(),
    &device::Button::stateChanged,
    m_ui->doorLed,
    [this](bool state) { state ? m_ui->doorLed->on() : m_ui->doorLed->off(); });

  connect(m_powerButton.get(),
          &device::Button::stateChanged,
          m_ui->powerLed,
          [this](bool state) {
            state ? m_ui->powerLed->on() : m_ui->powerLed->off();
          });

  connect(m_diagSensor.get(),
          &device::Button::stateChanged,
          m_ui->kled,
          [this](bool state) {
            QLOG_INFO() << this << "DIAG STATE CHANGED" << state;
            state ? m_ui->kled->on() : m_ui->kled->off();
          });

  connect(m_ui->doorRefresh, &QPushButton::clicked, this, [this] {
    m_executor.get().execute<cmd::motherboard::ButtonsRequestDoor>(
      m_doorButton);
  });

  connect(m_ui->sleevesUpdate, &QPushButton::clicked, this, [this] {
    m_executor.get().execute<cmd::motherboard::DiagOptStateOperation>(
      m_diagSensor);
  });

  connect(m_ui->powerButtonRefresh, &QPushButton::clicked, this, [this] {
    m_executor.get().execute<cmd::motherboard::ButtonsRequestPowerButton>(
      m_powerButton);
  });

  connect(m_ui->sleevesResetTrigger, &QPushButton::clicked, this, [this] {
    //    m_executor.get().execute<cmd::motherboard::DiagOptState>(
    //      m_buttons);
  });
}

void
ButtonsControl::pollRoutine()
{
  m_executor.get().execute<cmd::motherboard::PowerGetButtonsOperation>(
    m_doorButton, m_powerButton);

  m_executor.get().execute<cmd::motherboard::DiagOptStateOperation>(
    m_diagSensor);
}

} // namespace screen
