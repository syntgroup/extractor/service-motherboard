#ifndef SCREEN_DEVICE_CONTROL_H
#define SCREEN_DEVICE_CONTROL_H

#include <QWidget>

namespace transfer {
class Executor;
}

namespace device {
class MotherboardParameters;
}

namespace screen {

namespace Ui {
class DeviceControl;
} // namespace Ui

class DeviceControl : public QWidget
{
  Q_OBJECT

public:
  explicit DeviceControl(transfer::Executor& executor,
                         QWidget* parent = nullptr);
  ~DeviceControl() override;

private:
  std::reference_wrapper<transfer::Executor> m_executor;

  std::unique_ptr<Ui::DeviceControl> m_ui{ nullptr };
  std::shared_ptr<device::MotherboardParameters> m_parameters;

  void setupConnections();
};

} // namespace screen
#endif  // SCREEN_DEVICE_CONTROL_H
