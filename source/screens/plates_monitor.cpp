#include "plates_monitor.h"
#include "ui_plates_monitor.h"

#include "motherboard/cmd/sns_state_operation.h"
#include "motherboard/device/ir_sensors.h"
#include "transfer/executor.h"
#include <KLed>
#include <QColor>
#include <QTimer>

namespace screen {

PlatesMonitor::PlatesMonitor(transfer::Executor& executor, QWidget* parent)
  : QWidget{ parent }
  , m_executor{ executor }
  , m_ui{ std::make_unique<Ui::PlatesMonitor>() }
  , m_pollTimer{ std::make_unique<QTimer>() }
  , m_irSensors{ std::make_shared<device::IrSensors>() }
{
  m_ui->setupUi(this);
  m_pollTimer->setSingleShot(false);

  setupConnections();
}

PlatesMonitor::~PlatesMonitor() = default;

void
PlatesMonitor::setupConnections()
{
  const std::array leds{
    m_ui->kled, m_ui->kled_2, m_ui->kled_3, m_ui->kled_4, m_ui->kled_5
  };
  //  for (int i = 0; i < 5; ++i) {
  //    auto led_widget{ std::make_unique<KLed>() };
  //    led_widget->setProperty("index", i);

  //    auto* led{ motherboard()->led(i) };
  //    connect(
  //      led,
  //      &device::RgbLed::colorChanged,
  //      led,
  //      [&led_widget](device::Color color) {
  //        auto new_color{ utils::ColorConverter::mapDeviceColorToQColor(color)
  //        }; led_widget->setColor(new_color);
  //      });

  //    m_ui->plates_box->layout()->addWidget(led_widget.get());
  //    m_leds.emplace_back(std::move(led_widget));
  //  }

  connect(
    m_pollTimer.get(), &QTimer::timeout, this, &PlatesMonitor::pollRoutine);
  connect(m_ui->pull_button,
          &QToolButton::clicked,
          this,
          &PlatesMonitor::pollRoutine);

  connect(m_ui->poll_auto, &QToolButton::toggled, this, [this](bool state) {
    m_ui->pull_button->setEnabled(!state);
    state ? m_pollTimer->start(m_ui->pollIntervalSpinBox->value())
          : m_pollTimer->stop();
  });

  connect(m_ui->pollIntervalSpinBox,
          QOverload<int>::of(&QSpinBox::valueChanged),
          m_pollTimer.get(),
          QOverload<int>::of(&QTimer::setInterval));
}

void
PlatesMonitor::pollRoutine()
{
  m_executor.get().execute<cmd::motherboard::SnsStateOperation>(m_irSensors,
                                                                (uint)0);
}

} // namespace screen
