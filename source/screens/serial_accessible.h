#ifndef SCREEN_SERIALACCESSIBLE_H
#define SCREEN_SERIALACCESSIBLE_H

#include "di/i_consumer.h"

namespace device
{
class Motherboard;
}  // namespace device

namespace transfer
{
class SrvIntExecutor;
}  // namespace transfer

namespace screen {

class ISerialAccessible
  : public di::IConsumer<device::Motherboard>
  , public di::IConsumer<transfer::SrvIntExecutor>
{
protected:
  virtual void setupConnections() = 0;

  [[nodiscard]] auto motherboard() const
  {
    return di::IConsumer<device::Motherboard>::m_data;
  }

  [[nodiscard]] auto executor() const
  {
    return di::IConsumer<transfer::SrvIntExecutor>::m_data;
  }
};

} // namespace screen

#endif // SCREEN_SERIALACCESSIBLE_H
