#ifndef SCREEN_CONNECTION_CONTROL_H
#define SCREEN_CONNECTION_CONTROL_H

#include <QSerialPort>
#include <QWidget>
#include <memory>

class QSerialPort;

namespace transfer {
class SrvIntSession;
class Executor;
}

namespace screen {

namespace Ui {
class ConnectionControl;
} // namespace Ui

class ConnectionControl : public QWidget
{
  Q_OBJECT

public:
  explicit ConnectionControl(transfer::SrvIntSession& session,
                             QWidget* parent = nullptr);
  ~ConnectionControl() override;

private:
  const std::reference_wrapper<transfer::SrvIntSession> m_session;
  std::unique_ptr<Ui::ConnectionControl> m_ui{ nullptr };

  enum pos
  {
    port_name = 0,
    port_description,
    port_manufacturer,
    port_serial_number,
    port_location,
    port_vendor_id,
    port_product_id
  };

  struct settings
  {
    QString name;
    QSerialPort::BaudRate baud_rate {QSerialPort::Baud115200};
    QSerialPort::DataBits data_bits {QSerialPort::Data8};
    QSerialPort::Parity parity {QSerialPort::NoParity};
    QSerialPort::StopBits stop_bits {QSerialPort::OneStop};
    QSerialPort::FlowControl flow_control {QSerialPort::NoFlowControl};
    bool local_echo_enabled {false};
  };
  settings m_current_settings;

  void fillPortsParameters();
  void fillPortsInfo();
  void updateSettings();

public slots:
  void port_connect();
  void port_disconnect();

private slots:
  void showPortInfo(int idx);

  void on_portConnected();
  void on_portDisconnected();

  void on_motherboard_port_connected(bool state);
};

} // namespace screen

#endif  // SCREEN_CONNECTION_CONTROL_H
