#ifndef SCREEN_VOLTAGES_CONTROL_H
#define SCREEN_VOLTAGES_CONTROL_H

#include <QWidget>

namespace transfer {
class Executor;
} // namespace transfer

namespace device {
class Powers;
class UvLamp;
} // namespace device

namespace screen
{

namespace Ui {
class VoltagesControl;
} // namespace Ui

class VoltagesControl : public QWidget
{
  Q_OBJECT
  Q_DISABLE_COPY_MOVE(VoltagesControl)

public:
  explicit VoltagesControl(transfer::Executor& executor,
                           QWidget* parent = nullptr);
  ~VoltagesControl() override;

private:
  std::reference_wrapper<transfer::Executor> m_executor;

  std::unique_ptr<Ui::VoltagesControl> m_ui{ nullptr };

  std::shared_ptr<device::Powers> m_powers;
  std::shared_ptr<device::UvLamp> m_uvLamp;

  void setupConnections();
};

}  // namespace screen
#endif // SCREEN_VOLTAGES_CONTROL_H
