#include "voltages_control.h"
#include "ui_voltages_control.h"

#include "motherboard/cmd/power_get_24v_operation.h"
#include "motherboard/cmd/voltage_get_uv_operation.h"
#include "motherboard/device/lamp_uv.h"
#include "motherboard/device/powers.h"
#include "transfer/executor.h"

namespace screen {

VoltagesControl::VoltagesControl(transfer::Executor& executor, QWidget* parent)
  : QWidget{ parent }
  , m_executor{ executor }
  , m_ui{ std::make_unique<Ui::VoltagesControl>() }
  , m_powers{ std::make_shared<device::Powers>() }
  , m_uvLamp{ std::make_shared<device::UvLamp>() }
{
  m_ui->setupUi(this);
  setupConnections();
}

VoltagesControl::~VoltagesControl() = default;

void
VoltagesControl::setupConnections()
{
  connect(m_ui->b_get24V, &QPushButton::clicked, this, [this] {
    m_executor.get().execute<cmd::motherboard::VoltageGet24VOperation>(
      m_powers);
  });
  connect(m_ui->b_getUV, &QPushButton::clicked, this, [this] {
    m_executor.get().execute<cmd::motherboard::VoltageGetUvOperation>(m_uvLamp);
  });

  connect(m_powers.get(),
          &device::Powers::uvRailVoltageChanged,
          m_ui->lcd_uvRail,
          [this](device::Voltage value) {
            m_ui->lcd_uvRail->display(QString::number(value.value_of()));
          });

  connect(m_powers.get(),
          &device::Powers::v24RailVoltageChanged,
          m_ui->lcd24Rail,
          [this](device::Voltage value) {
            m_ui->lcd24Rail->display(value.value_of());
          });
}

} // namespace screen
