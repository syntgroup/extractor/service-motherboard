#ifndef SCREEN_ENGINE_CONTROL_H
#define SCREEN_ENGINE_CONTROL_H

#include <QComboBox>
#include <QVariant>
#include <QWidget>
#include <optional>

namespace device {
class Engine;
class Engines;
class Motherboard;
} // namespace device

namespace transfer {
class Executor;
}

namespace models {
class EnginesParams;
} // namespace models

namespace screen {

namespace Ui {
class EngineControl;
} // namespace Ui

class EngineControl : public QWidget
{
  Q_OBJECT
  Q_DISABLE_COPY_MOVE(EngineControl);

public:
  explicit EngineControl(transfer::Executor& executor,
                         QWidget* parent = nullptr);
  ~EngineControl() override;

  // QObject interface
  bool eventFilter(QObject* watched, QEvent* event) override;

private:
  std::reference_wrapper<transfer::Executor> m_executor;

  std::unique_ptr<Ui::EngineControl> m_ui{ nullptr };
  std::unique_ptr<QTimer> m_mixTimer{ nullptr };
  std::unique_ptr<device::Engines> m_engines;


  void setupConnections();
  [[nodiscard]] auto currentEngine() const -> std::optional<device::Engine*>;

  std::chrono::seconds m_mixTime{ 0 };

private slots:
  void engineSelected(int drv_idx);
  void mixTimerRoutine();
};

} // namespace screen

#endif // SCREEN_ENGINE_CONTROL_H
