#ifndef SCREEN_PLATES_MONITOR_H
#define SCREEN_PLATES_MONITOR_H

#include <QWidget>

class QTimer;

namespace transfer {
class Executor;
}

namespace device {
class Motherboard;
class IrSensors;
}

namespace screen {

namespace Ui {
class PlatesMonitor;
}

class PlatesMonitor : public QWidget
{
  Q_OBJECT

public:
  explicit PlatesMonitor(transfer::Executor& executor,
                         QWidget* parent = nullptr);
  ~PlatesMonitor();

private:
  std::reference_wrapper<transfer::Executor> m_executor;

  std::unique_ptr<Ui::PlatesMonitor> m_ui{ nullptr };
  std::unique_ptr<QTimer> m_pollTimer{ nullptr };
  std::shared_ptr<device::IrSensors> m_irSensors;

  // SerialAccessible interface
protected:
  void setupConnections();

private slots:
  void pollRoutine();
};

} // namespace screen

#endif // SCREEN_PLATES_MONITOR_H
