#include "power_control.h"
#include "ui_power_control.h"

#include "motherboard/cmd/power_set_buzzer_operation.h"
#include "motherboard/cmd/power_set_led_operation.h"
#include "motherboard/cmd/power_set_uv_lamp_operation.h"
#include "motherboard/device/lamp_led.h"
#include "motherboard/device/lamp_uv.h"
#include "motherboard/device/motherboard_values.h"
#include "transfer/executor.h"
#include <QIcon>

namespace screen {

PowerControl::PowerControl(transfer::Executor& executor, QWidget* parent)
  : QWidget{ parent }
  , m_executor{ executor }
  , m_ui{ std::make_unique<Ui::PowerControl>() }
  , m_lampLed{ std::make_shared<device::LedLamp>() }
  , m_lampUv{ std::make_shared<device::UvLamp>() }
{
  m_ui->setupUi(this);

  const std::array buttons{ m_ui->b_buzzerOn, m_ui->b_uvOn };
  std::for_each(buttons.cbegin(), buttons.cend(), [this](auto* button) {
    toggleButton(*button, /*state=*/false);
  });

  setupConnections();
}

void
PowerControl::toggleButton(QPushButton& button, bool state)
{
  const QIcon on_icon{ ":/silk/icons/lightbulb.png" };
  const QIcon off_icon{ ":/silk/icons/lightbulb_off.png" };

  button.setIcon(state ? on_icon : off_icon);
  button.setText(state ? QStringLiteral("online") : QStringLiteral("offline"));
}

PowerControl::~PowerControl() = default;

void
PowerControl::setupConnections()
{
  connect(m_ui->d_ledPWM, &QDial::valueChanged, this, [this](int value) {
    if (value == m_ui->d_ledPWM->minimum()) {
      m_executor.get().execute<cmd::motherboard::PowerSetLedOperation>(
        m_lampLed,
        device::PwmLevel{ static_cast<uint16_t>(m_ui->d_ledPWM->minimum()) });
    } else if (value == m_ui->d_ledPWM->maximum()) {
      m_executor.get().execute<cmd::motherboard::PowerSetLedOperation>(
        m_lampLed,
        device::PwmLevel{ static_cast<uint16_t>(m_ui->d_ledPWM->maximum()) });
    } else {
      m_executor.get().execute<cmd::motherboard::PowerSetLedOperation>(
        m_lampLed, device::PwmLevel{ static_cast<uint16_t>(value) });
    }
  });

  connect(m_ui->b_uvOn, &QPushButton::toggled, this, [this](bool status) {
    m_executor.get().execute<cmd::motherboard::PowerSetUvLampOperation>(
      status, m_lampUv);
    toggleButton(*m_ui->b_uvOn, status);
  });

  connect(m_ui->b_buzzerOn, &QPushButton::toggled, this, [this](bool status) {
    m_executor.get().execute<cmd::motherboard::PowerSetBuzzerOperation>(status);
    toggleButton(*m_ui->b_buzzerOn, status);
  });
}

} // namespace screen
