#include "connection_control.h"
#include "ui_connection_control.h"

#include "transfer/srvintsession.h"
#include <QSerialPortInfo>
#include <QsLog/QsLog.h>

namespace {
constexpr auto blank_string{ "N/A" };

constexpr auto default_baudrate{ QSerialPort::Baud115200 };
constexpr std::array baudrates{ std::make_pair(QSerialPort::Baud9600, "9600"),
                                std::make_pair(QSerialPort::Baud19200, "19200"),
                                std::make_pair(QSerialPort::Baud38400, "38400"),
                                std::make_pair(QSerialPort::Baud115200,
                                               "115200") };

constexpr auto default_data_bit{ QSerialPort::Data8 };
constexpr std::array data_butts{ std::make_pair(5, QSerialPort::Data5),
                                 std::make_pair(6, QSerialPort::Data6),
                                 std::make_pair(7, QSerialPort::Data7),
                                 std::make_pair(8, QSerialPort::Data8) };

constexpr auto default_parity{ QSerialPort::NoParity };
constexpr std::array parities{ std::make_pair("None", QSerialPort::NoParity),
                               std::make_pair("Even", QSerialPort::EvenParity),
                               std::make_pair("Odd", QSerialPort::OddParity),
                               std::make_pair("Mark", QSerialPort::MarkParity),
                               std::make_pair("Space",
                                              QSerialPort::SpaceParity) };

constexpr auto default_stop_bits{ QSerialPort::OneStop };
constexpr std::array stop_bits{ std::make_pair(1, QSerialPort::OneStop),
#ifdef Q_OS_WIN
                                std::make_pair(1.5,
                                               QSerialPort::OneAndHalfStop),
#endif
                                std::make_pair(2, QSerialPort::TwoStop) };
}

namespace screen {

ConnectionControl::ConnectionControl(transfer::SrvIntSession& session,
                                     QWidget* parent)
  : QWidget{ parent }
  , m_session{ session }
  , m_ui{ std::make_unique<Ui::ConnectionControl>() }
{
  m_ui->setupUi(this);
  //  m_ui->parametersBox->setEnabled(false);
  //  m_ui->additionalOptionsGroupBox->setEnabled(false);

  m_ui->baudRateBox->setInsertPolicy(QComboBox::NoInsert);

  connect(m_ui->serialPortInfoListBox,
          QOverload<int>::of(&QComboBox::currentIndexChanged),
          this,
          &ConnectionControl::showPortInfo);

  connect(m_ui->b_connect,
          &QPushButton::clicked,
          this,
          &ConnectionControl::port_connect);
  connect(m_ui->b_disconnect,
          &QPushButton::clicked,
          this,
          &ConnectionControl::port_disconnect);

  connect(m_ui->refreshButton,
          &QToolButton::clicked,
          this,
          &ConnectionControl::fillPortsInfo);

  fillPortsParameters();
  fillPortsInfo();

  updateSettings();
}

ConnectionControl::~ConnectionControl() = default;

void
ConnectionControl::showPortInfo(int idx)
{
  if (idx == -1) {
    return;
  }

  const QStringList list{
    m_ui->serialPortInfoListBox->itemData(idx).toStringList()
  };

  m_ui->descriptionLabel->setText(QStringLiteral("Description: %1")
                                    .arg(list.count() > port_description
                                           ? list.at(port_description)
                                           : blank_string));
  m_ui->manufacturerLabel->setText(QStringLiteral("Manufacturer: %1")
                                     .arg(list.count() > port_manufacturer
                                            ? list.at(port_manufacturer)
                                            : blank_string));
  m_ui->serialNumberLabel->setText(QStringLiteral("Serial number: %1")
                                     .arg(list.count() > port_serial_number
                                            ? list.at(port_serial_number)
                                            : blank_string));
  m_ui->locationLabel->setText(QStringLiteral("Location: %1")
                                 .arg(list.count() > port_location
                                        ? list.at(port_location)
                                        : blank_string));
  m_ui->vidLabel->setText(QStringLiteral("Vendor Identifier: %1")
                            .arg(list.count() > port_vendor_id
                                   ? list.at(port_vendor_id)
                                   : blank_string));
  m_ui->pidLabel->setText(QStringLiteral("Product Identifier: %1")
                            .arg(list.count() > port_product_id
                                   ? list.at(port_product_id)
                                   : blank_string));
}

void
ConnectionControl::port_connect()
{
  updateSettings();
  QLOG_INFO() << this << "port connect" << m_current_settings.name;

  m_session.get().start(m_current_settings.name);
}

void
ConnectionControl::port_disconnect()
{
  m_session.get().stop();
}

void
ConnectionControl::on_motherboard_port_connected(bool state)
{
  if (state) {
    on_portConnected();
  } else {
    on_portDisconnected();
  }
}

void
ConnectionControl::fillPortsParameters()
{
  for (auto&& baudrate : baudrates) {
    m_ui->baudRateBox->addItem(std::get<const char*>(baudrate),
                               std::get<QSerialPort::BaudRate>(baudrate));
  }
  m_ui->baudRateBox->setCurrentIndex(
    m_ui->baudRateBox->findData(default_baudrate));

  for (auto&& data_bit : data_butts) {
    m_ui->dataBitsBox->addItem(QString::number(std::get<int>(data_bit)),
                               std::get<QSerialPort::DataBits>(data_bit));
  }
  m_ui->dataBitsBox->setCurrentIndex(
    m_ui->dataBitsBox->findData(default_data_bit));

  for (auto&& parity : parities) {
    m_ui->parityBox->addItem(std::get<const char*>(parity),
                             std::get<QSerialPort::Parity>(parity));
  }
  m_ui->parityBox->setCurrentIndex(m_ui->parityBox->findData(default_parity));

  for (auto&& stop_bit : stop_bits) {
    m_ui->stopBitsBox->addItem(QString::number(std::get<int>(stop_bit)),
                               std::get<QSerialPort::StopBits>(stop_bit));
  }
  m_ui->stopBitsBox->setCurrentIndex(
    m_ui->stopBitsBox->findData(default_stop_bits));

  m_ui->flowControlBox->addItem(QStringLiteral("None"),
                                QSerialPort::NoFlowControl);
  m_ui->flowControlBox->addItem(QStringLiteral("RTS/CTS"),
                                QSerialPort::HardwareControl);
  m_ui->flowControlBox->addItem(QStringLiteral("XON/XOFF"),
                                QSerialPort::SoftwareControl);
  m_ui->flowControlBox->setCurrentIndex(
    m_ui->flowControlBox->findData(QSerialPort::NoFlowControl));
}

void
ConnectionControl::fillPortsInfo()
{
  m_ui->serialPortInfoListBox->clear();
  QString description;
  QString manufacturer;
  QString serialNumber;
  const auto infos = QSerialPortInfo::availablePorts();
  for (const QSerialPortInfo& info : infos) {
    description  = info.description();
    manufacturer = info.manufacturer();
    serialNumber = info.serialNumber();
    const QStringList list{
      info.portName(),
      (!description.isEmpty() ? description : blank_string),
      (!manufacturer.isEmpty() ? manufacturer : blank_string),
      (!serialNumber.isEmpty() ? serialNumber : blank_string),
      info.systemLocation(),
      (info.vendorIdentifier() ? QString::number(info.vendorIdentifier(), 16)
                               : blank_string),
      (info.productIdentifier() ? QString::number(info.productIdentifier(), 16)
                                : blank_string)
    };

    m_ui->serialPortInfoListBox->addItem(list.first(), list);
  }

  m_ui->serialPortInfoListBox->addItem(QStringLiteral("Custom"));
}

void
ConnectionControl::updateSettings()
{
  m_current_settings.name      = m_ui->serialPortInfoListBox->currentText();
  m_current_settings.baud_rate = static_cast<QSerialPort::BaudRate>(
    m_ui->baudRateBox->itemData(m_ui->baudRateBox->currentIndex()).toInt());

  m_current_settings.data_bits = static_cast<QSerialPort::DataBits>(
    m_ui->dataBitsBox->itemData(m_ui->dataBitsBox->currentIndex()).toInt());

  m_current_settings.parity = static_cast<QSerialPort::Parity>(
    m_ui->parityBox->itemData(m_ui->parityBox->currentIndex()).toInt());

  m_current_settings.stop_bits = static_cast<QSerialPort::StopBits>(
    m_ui->stopBitsBox->itemData(m_ui->stopBitsBox->currentIndex()).toInt());

  m_current_settings.flow_control = static_cast<QSerialPort::FlowControl>(
    m_ui->flowControlBox->itemData(m_ui->flowControlBox->currentIndex())
      .toInt());

  m_current_settings.local_echo_enabled = m_ui->localEchoCheckBox->isChecked();
}

void
ConnectionControl::on_portConnected()
{
  m_ui->b_connect->setEnabled(false);
  m_ui->b_disconnect->setEnabled(true);
}

void
ConnectionControl::on_portDisconnected()
{
  m_ui->b_connect->setEnabled(true);
  m_ui->b_disconnect->setEnabled(false);
}

} // namespace screen
