#ifndef I_SCREEN_H
#define I_SCREEN_H

#include <KLocalizedString>
#include <QIcon>
#include <QWidget>

namespace screen {

class IScreen : public QWidget
{
  Q_OBJECT

public:
  using QWidget::QWidget;

  [[nodiscard]] virtual QIcon icon() const            = 0;
  [[nodiscard]] virtual QString sectionHeader() const = 0;
  [[nodiscard]] virtual QString pageHeader() const    = 0;
};

}

#endif // I_SCREEN_H
