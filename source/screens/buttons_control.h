#ifndef SCREEN_BUTTONS_CONTROL_H
#define SCREEN_BUTTONS_CONTROL_H

#include <QWidget>

class QTimer;

namespace transfer {
class Executor;
}

namespace device {
class Motherboard;
class Button;
}

namespace screen {

namespace Ui {
class ButtonsControl;
}

class ButtonsControl : public QWidget
{
  Q_OBJECT

public:
  explicit ButtonsControl(transfer::Executor& executor,
                          QWidget* parent = nullptr);
  ~ButtonsControl() override;

protected:
  void setupConnections();

private:
  std::reference_wrapper<transfer::Executor> m_executor;

  std::unique_ptr<Ui::ButtonsControl> m_ui{ nullptr };
  std::unique_ptr<QTimer> m_pollTimer{ nullptr };

  std::shared_ptr<device::Button> m_doorButton;
  std::shared_ptr<device::Button> m_powerButton;
  std::shared_ptr<device::Button> m_diagSensor;

private slots:
  void pollRoutine();
};

} // namespace screen
#endif // SCREEN_BUTTONS_CONTROL_H
