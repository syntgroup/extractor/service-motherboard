#include "engine_control.h"
#include "ui_engine_control.h"

#include "models/current_scaling_model.h"
#include "models/microstep_model.h"
#include "motherboard/cmd/engine_driver_factory_reset.h"
#include "motherboard/cmd/engine_driver_raw_operation.h"
#include "motherboard/cmd/engine_enable_operation.h"
#include "motherboard/cmd/engine_get_acceleration.h"
#include "motherboard/cmd/engine_get_currents_operation.h"
#include "motherboard/cmd/engine_get_max_speed.h"
#include "motherboard/cmd/engine_get_microstep.h"
#include "motherboard/cmd/engine_get_mix_mode_status_operation.h"
#include "motherboard/cmd/engine_get_position_current.h"
#include "motherboard/cmd/engine_get_position_home.h"
#include "motherboard/cmd/engine_get_repeatability.h"
#include "motherboard/cmd/engine_write_acceleration_operation.h"
#include "motherboard/cmd/engine_write_max_speed_operation.h"
#include "motherboard/cmd/engine_write_microstep_operation.h"
#include "motherboard/cmd/engine_write_position_home_operation.h"
#include "motherboard/cmd/engine_write_repeatability_operation.h"
#include "motherboard/cmd/goto_em_stop_operation.h"
#include "motherboard/cmd/goto_mix_cycles_operation.h"
#include "motherboard/cmd/goto_mix_stop_operation.h"
#include "motherboard/cmd/goto_mix_time_operation.h"
#include "motherboard/cmd/goto_n_ticks_operation.h"
#include "motherboard/cmd/goto_n_ticks_unc_wo_operation.h"
#include "motherboard/cmd/goto_sm_stop_operation.h"
#include "motherboard/cmd/goto_write_currents_operation.h"
#include "motherboard/cmd/multi_goto_absolute_operation.h"
#include "motherboard/cmd/multi_goto_home_position_operation.h"
#include "motherboard/cmd/multi_mix_n_seconds_operation.h"
#include "motherboard/device/engine.h"
#include "motherboard/device/engine_defaults.h"
#include "motherboard/device/engine_values.h"
#include "motherboard/device/engines.h"
#include "transfer/executor.h"
#include <QTimer>
#include <QsLog/QsLog.h>
#include <gsl/gsl-lite.hpp>

namespace {

template<class TWidget, typename T>
  requires std::is_same_v<TWidget, QSpinBox> ||
           std::is_same_v<TWidget, QDoubleSpinBox>
void
modify_widget_value(TWidget& spinbox, T value)
{
  spinbox.setValue(value);
}

template<class TWidget, typename T>
  requires std::is_same_v<TWidget, QComboBox> && std::is_same_v<T, QVariant>
void
modify_widget_value(TWidget& combobox, T value)
{
  combobox.setCurrentIndex(combobox.findData(value, Qt::EditRole));
}

template<class TWidget, typename T>
  requires std::is_same_v<TWidget, QComboBox> && std::is_same_v<T, int>
void
modify_widget_value(TWidget& combobox, T value)
{
  combobox->setCurrentIndex(combobox.findData(value, Qt::UserRole));
}

[[nodiscard]] auto
frequency_get(const device::Engine& engine, device::FrequencyType type)
{
  switch (type) {
    case device::FrequencyType::F0:
      return engine.accelerationProfile(device::AccelerationCurve::Normal)
        ->f0()
        .first.value_of();
    case device::FrequencyType::Fmax:
      return engine.accelerationProfile(device::AccelerationCurve::Normal)
        ->fmax()
        .first.value_of();
    case device::FrequencyType::dFmax:
      return engine.accelerationProfile(device::AccelerationCurve::Normal)
        ->dfmax()
        .first.value_of();
  }
}

constexpr std::chrono::milliseconds timer_poll_interval{ 1000 };
constexpr device::SpeedIndex zero_speed_index{ 0 };
const QTime zero_time{ 0, 0, 0, 0 };

} // namespace

namespace screen {

EngineControl::EngineControl(transfer::Executor& executor, QWidget* parent)
  : QWidget{ parent }
  , m_executor{ executor }
  , m_ui{ std::make_unique<Ui::EngineControl>() }
  , m_mixTimer{ std::make_unique<QTimer>() }
  , m_engines{ std::make_unique<device::Engines>() }
{
  m_ui->setupUi(this);

  m_ui->lcd_curentPosition->installEventFilter(this);
  m_ui->lcd_currentCycle->installEventFilter(this);

  m_ui->cb_microstep->setModel(new models::MicrostepModel{ this });
  m_ui->cb_currentAccel->setModel(new models::CurrentScalingModel{ this });
  m_ui->cb_currentStdby->setModel(new models::CurrentScalingModel{ this });

  setupConnections();

  for (auto axis : defaults::EngineParams::axises) {
    auto* engine{ m_engines->engineByAxis(axis).value() };
    const auto title{ QStringLiteral("%1: #0x%2")
                        .arg(engine->name(),
                             QString::number(engine->address(), 16)) };
    m_ui->cb_engineSelect->addItem(title, engine->axis());
  }

  m_ui->cb_engineSelect->setCurrentIndex(0);

  m_mixTimer->setInterval(timer_poll_interval);
  m_mixTimer->setSingleShot(false);
  connect(
    m_mixTimer.get(), &QTimer::timeout, this, &EngineControl::mixTimerRoutine);
}

EngineControl::~EngineControl() = default;

void
EngineControl::setupConnections()
{
  connect(m_ui->cb_engineSelect,
          QOverload<int>::of(&QComboBox::currentIndexChanged),
          this,
          &EngineControl::engineSelected);

  auto engine_option = currentEngine();
  if (!engine_option.has_value()) {
    return;
  }
  auto* engine = engine_option.value();

  const std::array fields{ std::make_pair(m_ui->le_drvctrl, m_ui->b_drvctrl),
                           std::make_pair(m_ui->le_chopconf, m_ui->b_chopconf),
                           std::make_pair(m_ui->le_smarten, m_ui->b_smarten),
                           std::make_pair(m_ui->le_sgsconf, m_ui->b_sgsconf),
                           std::make_pair(m_ui->le_drvconf, m_ui->b_drvconf) };
  std::for_each(
    fields.cbegin(), fields.cend(), [this, engine](const auto& writer) {
      connect(
        writer.second,
        &QPushButton::clicked,
        writer.second,
        [this, writer, engine] {
          const auto packet{ QByteArray::fromHex(
            writer.first->text().simplified().toUtf8()) };
          if (writer.first->hasAcceptableInput()) {
            m_executor.get()
              .execute<cmd::motherboard::EngineDriverRawWriteOperation>(engine,
                                                                        packet);
          }
        });
    });

  // ---------------

  connect(m_ui->b_smStop, &QPushButton::clicked, this, [this, engine] {
    cmd::motherboard::GotoSmStopOperation::Param params{ engine };
    m_executor.get().execute<cmd::motherboard::GotoSmStopOperation>(
      std::move(params));
  });
  connect(m_ui->b_emStop, &QPushButton::clicked, this, [this, engine] {
    cmd::motherboard::GotoEmStopOperation::Param params{ engine };
    m_executor.get().execute<cmd::motherboard::GotoEmStopOperation>(
      std::move(params));
  });

  connect(m_ui->b_turnOff, &QPushButton::clicked, this, [this, engine] {
    m_executor.get().execute<cmd::motherboard::EngineEnableOperation>(engine,
                                                                      false);
  });
  connect(m_ui->b_turnOn, &QPushButton::clicked, this, [this, engine] {
    m_executor.get().execute<cmd::motherboard::EngineEnableOperation>(engine,
                                                                      true);
  });

  connect(m_ui->b_goZero, &QPushButton::clicked, this, [this, engine] {
    m_executor.get().execute<cmd::multi::GotoHomeOperation>(engine);
  });

  connect(m_ui->b_goAbsolute, &QPushButton::clicked, this, [this, engine] {
    const device::Coordinate target{ m_ui->sb_coordinate->value() };
    m_executor.get().execute<cmd::multi::GotoAbsoluteOperation>(
      engine, target, zero_speed_index);
  });

  connect(m_ui->b_goLeft, &QPushButton::clicked, this, [this, engine] {
    const device::Coordinate target{ m_ui->sb_coordinate->value() * -1 };
    m_executor.get().execute<cmd::motherboard::GotoNTicksOperation>(engine,
                                                                    target);
  });

  connect(m_ui->b_goRight, &QPushButton::clicked, this, [this, engine] {
    const device::Coordinate target{ m_ui->sb_coordinate->value() };
    m_executor.get().execute<cmd::motherboard::GotoNTicksOperation>(engine,
                                                                    target);
  });

  connect(m_ui->b_goLeftUNC, &QPushButton::clicked, this, [this, engine] {
    const device::Coordinate target{ m_ui->sb_coordinate->value() * -1 };
    m_executor.get().execute<cmd::motherboard::GotoNTicksUncWoOperation>(
      engine, target);
  });

  connect(m_ui->b_goRightUNC, &QPushButton::clicked, this, [this, engine] {
    const device::Coordinate target{ m_ui->sb_coordinate->value() };
    m_executor.get().execute<cmd::motherboard::GotoNTicksUncWoOperation>(
      engine, target);
  });

  connect(
    m_ui->b_startMix, &QPushButton::clicked, this, [this, engine] {
      const device::Cycles cycles{ static_cast<uint32_t>(
        m_ui->sb_mixTime->value()) };
      const device::Coordinate amlitude{ static_cast<uint32_t>(
        m_ui->sb_mixAmplitude->value()) };
      // m_executor.get().executeMulti<cmd::multi::MixNSecondsOperation>(
      // engine, cycles, amlitude, speed);
      m_executor.get().execute<cmd::motherboard::GotoMixCyclesOperation>(
        engine, cycles, amlitude);
    });
  connect(m_ui->b_stopMix, &QPushButton::clicked, this, [this, engine] {
    m_executor.get().execute<cmd::motherboard::GotoMixStopOperation>(engine);
  });

  connect(m_ui->b_startMixTime, &QPushButton::clicked, this, [this, engine] {
    const auto time{ static_cast<uint32_t>(m_ui->timeEdit->time().second()) };
    const device::Coordinate amplitude{ m_ui->spinBox_2->value() };

    m_ui->lcdNumber->display(zero_time.addSecs(time).toString("mm:ss"));

    m_mixTime = std::chrono::seconds{ time };
    m_mixTimer->start();

    m_executor.get().execute<cmd::motherboard::GotoMixTimeOperation>(
      engine, m_mixTime, amplitude);
  });
  connect(m_ui->b_stopMixTime, &QPushButton::clicked, this, [this, engine] {
    m_executor.get().execute<cmd::motherboard::GotoMixStopOperation>(engine);
  });

  connect(m_ui->b_pullDFmax, &QPushButton::clicked, this, [this, engine] {
    m_executor.get().execute<cmd::motherboard::EngineGetAccelerationOperation>(
      engine, device::AccelerationCurve::Normal, device::FrequencyType::dFmax);
  });
  connect(
    m_ui->b_pushDFmax, &QPushButton::clicked, this, [this, engine] {
      const device::Acceleration value{ device::Frequency{
                                          m_ui->sb_dFmax->value() },
                                        device::FrequencyType::dFmax };
      m_executor.get()
        .execute<cmd::motherboard::EngineWriteAccelerationOperation>(engine,
                                                                     value);
    });

  connect(m_ui->b_pullF0, &QPushButton::clicked, this, [this, engine] {
    m_executor.get().execute<cmd::motherboard::EngineGetAccelerationOperation>(
      engine, device::AccelerationCurve::Normal, device::FrequencyType::F0);
  });
  connect(
    m_ui->b_pushF0, &QPushButton::clicked, this, [this, engine] {
      const device::Acceleration value{
        device::Frequency{ m_ui->sb_f0->value() }, device::FrequencyType::F0
      };
      m_executor.get()
        .execute<cmd::motherboard::EngineWriteAccelerationOperation>(engine,
                                                                     value);
    });

  connect(m_ui->b_pullFmax, &QPushButton::clicked, this, [this, engine] {
    m_executor.get().execute<cmd::motherboard::EngineGetAccelerationOperation>(
      engine, device::AccelerationCurve::Normal, device::FrequencyType::Fmax);
  });
  connect(
    m_ui->b_pushFmax, &QPushButton::clicked, this, [this, engine] {
      const device::Acceleration value{
        device::Frequency{ m_ui->sb_fmax->value() }, device::FrequencyType::Fmax
      };
      m_executor.get()
        .execute<cmd::motherboard::EngineWriteAccelerationOperation>(engine,
                                                                     value);
    });

  connect(m_ui->b_pullMaxSpeed, &QPushButton::clicked, this, [this, engine] {
    m_executor.get().execute<cmd::motherboard::EngineGetMaxSpeedOperation>(
      engine);
  });
  connect(m_ui->b_pushMaxSpeed, &QPushButton::clicked, this, [this, engine] {
    const device::SpeedIndex value{ m_ui->sb_maxSpeed->value() };
    m_executor.get().execute<cmd::motherboard::EngineWriteMaxSpeedOperation>(
      engine, value);
  });

  connect(m_ui->b_pullMicrostep, &QPushButton::clicked, this, [this, engine] {
    m_executor.get().execute<cmd::motherboard::EngineGetMicrostepOperation>(
      engine);
  });
  connect(m_ui->b_pushMicrostep, &QPushButton::clicked, this, [this, engine] {
    const auto value{ static_cast<device::Microstep>(
      m_ui->cb_microstep->currentData(Qt::EditRole).toUInt()) };
    m_executor.get().execute<cmd::motherboard::EngineWriteMicrostepOperation>(
      engine, value);
  });

  connect(
    m_ui->b_pullRepeatability, &QPushButton::clicked, this, [this, engine] {
      m_executor.get()
        .execute<cmd::motherboard::EngineGetRepeatabilityOperation>(engine);
    });
  connect(
    m_ui->b_pushRepeatability, &QPushButton::clicked, this, [this, engine] {
      const device::Repeatability value{ m_ui->sb_repeatability->value() };
      m_executor.get()
        .execute<cmd::motherboard::EngineWriteRepeatabilityOperation>(engine,
                                                                      value);
    });

  connect(m_ui->b_pullCurrents, &QPushButton::clicked, this, [this, engine] {
    m_executor.get().execute<cmd::motherboard::EngineGetCurrentsOperation>(
      engine);
  });
  connect(m_ui->b_pushCurrents, &QPushButton::clicked, this, [this, engine] {
    const auto accel{ static_cast<device::Current>(
      m_ui->cb_currentAccel->currentData(Qt::EditRole).toUInt()) };
    const auto stdby{ static_cast<device::Current>(
      m_ui->cb_currentStdby->currentData(Qt::EditRole).toUInt()) };

    QLOG_DEBUG() << this << "will write accel" << accel << "and standby"
                 << stdby;

    m_executor.get().execute<cmd::motherboard::GotoWriteCurrentsOperation>(
      engine, accel, stdby);
  });

  connect(m_ui->b_goLeftUNC, &QPushButton::clicked, this, [this] {
    //! @todo доделать
    const auto target{ m_ui->sb_delta->value() * -1 };
  });
  connect(m_ui->b_goRightUNC, &QPushButton::clicked, this, [this] {
    //! @todo доделать
    const auto target{ m_ui->sb_delta->value() };
  });

  connect(
    m_ui->b_pullHomePosition, &QPushButton::clicked, this, [this, engine] {
      m_executor.get()
        .execute<cmd::motherboard::EngineGetPositionHomeOperation>(engine);
    });
  connect(
    m_ui->b_pushHomePosition, &QPushButton::clicked, this, [this, engine] {
      const device::Coordinate value{ m_ui->sb_homePosition->value() };
      m_executor.get()
        .execute<cmd::motherboard::EngineWritePositionHomeOperation>(engine,
                                                                     value);
    });

  connect(m_ui->b_resetParams, &QPushButton::clicked, this, [this, engine] {
    m_executor.get().execute<cmd::motherboard::EngineDriverFactoryReset>(
      engine);
  });
}

std::optional<device::Engine*>
EngineControl::currentEngine() const
{
  return m_engines->engineByAxis(
    m_ui->cb_engineSelect->currentData(Qt::UserRole).toString());
}

void
EngineControl::engineSelected(int drv_idx)
{
  disconnect(m_ui->lcd_curentPosition);
  disconnect(m_ui->lcd_currentCycle);

  disconnect(m_ui->sb_dFmax);
  disconnect(m_ui->sb_f0);
  disconnect(m_ui->sb_fmax);
  disconnect(m_ui->sb_maxSpeed);
  disconnect(m_ui->sb_repeatability);
  disconnect(m_ui->cb_microstep);

  auto engine_option = currentEngine();
  if (!engine_option.has_value()) {
    QLOG_ERROR() << this << "engine selected" << drv_idx << "no engine";
    return;
  }
  auto* engine = engine_option.value();

  engine->zeroSwitch() == device::ZeroSwitch::ZeroIsSw1
    ? m_ui->rb_sw1->setChecked(true)
    : m_ui->rb_sw2->setChecked(true);

  connect(engine,
          &device::Engine::currentPositionChanged,
          m_ui->lcd_curentPosition,
          [this, engine](uint64_t pos) {
            QLOG_DEBUG() << this << engine->status().undefinedPos;
            engine->status().undefinedPos
              ? m_ui->lcd_curentPosition->display(QString::number(pos))
              : m_ui->lcd_curentPosition->display("--");
          });

  connect(engine,
          &device::Engine::currentMixCycleChanged,
          m_ui->lcd_currentCycle,
          [this](uint32_t cur_cycle) {
            m_ui->lcd_currentCycle->display(static_cast<int>(cur_cycle));
            m_ui->b_startMix->setEnabled(cur_cycle ==
                                         m_ui->sb_mixTime->value());
          });

  m_ui->lcd_curentPosition->display(
    QString::number(engine->currentPosition().value_of()));
  m_ui->lcd_currentCycle->display(
    QString::number(engine->currentMixCycle().value_of()));

  connect(
    engine,
    &device::Engine::frequencyChanged,
    this,
    [this](auto curve, auto value) {
      switch (value.second) {
        case device::FrequencyType::F0:
          m_ui->sb_f0->setValue(value.first.value_of());
          break;
        case device::FrequencyType::Fmax:
          m_ui->sb_fmax->setValue(value.first.value_of());
          break;
        case device::FrequencyType::dFmax:
          m_ui->sb_dFmax->setValue(value.first.value_of());
          break;
      }
    },
    Qt::QueuedConnection);
  connect(engine,
          &device::Engine::repeatabilityChanged,
          m_ui->sb_repeatability,
          QOverload<int>::of(&QSpinBox::setValue));
  connect(engine,
          &device::Engine::maxSpeedChanged,
          m_ui->sb_maxSpeed,
          QOverload<int>::of(&QSpinBox::setValue));

  connect(
    engine, &device::Engine::microstepChanged, this, [this](uint8_t value) {
      //      modifyComboboxValue(m_ui->cb_microstep, value);
      auto idx{ m_ui->cb_microstep->findData(value, Qt::UserRole) };
      m_ui->cb_microstep->setCurrentIndex(idx);
    });

  connect(
    engine,
    &device::Engine::currentScalingAccelChanged,
    this,
    //    [this](auto value) { modifyComboboxValue(m_ui->cb_currentAccel,
    //    value); });
    [this](auto value) {
      QLOG_DEBUG() << this << "will modify" << m_ui->cb_currentAccel << "with"
                   << value;
      auto idx{ m_ui->cb_currentAccel->findData(value, Qt::UserRole) };
      QLOG_DEBUG() << this << "found" << value << "at" << idx;
      m_ui->cb_currentAccel->setCurrentIndex(idx);
    },
    Qt::QueuedConnection);

  connect(
    engine,
    &device::Engine::currentScalingStdbyChanged,
    this,
    //    [this](auto value) { modifyComboboxValue(m_ui->cb_currentStdby,
    //    value); });
    [this](auto value) {
      QLOG_DEBUG() << this << "will modify" << m_ui->cb_currentStdby << "with"
                   << value;
      auto idx{ m_ui->cb_currentStdby->findData(value, Qt::UserRole) };
      QLOG_DEBUG() << this << "found" << value << "at" << idx;
      m_ui->cb_currentStdby->setCurrentIndex(idx);
    },
    Qt::QueuedConnection);

  connect(engine,
          &device::Engine::homePositionChanged,
          m_ui->sb_homePosition,
          [this](uint64_t value) {
            m_ui->sb_homePosition->setValue(static_cast<int>(value));
          });

  modify_widget_value(*m_ui->sb_f0,
                      ::frequency_get(*engine, device::FrequencyType::F0));
  modify_widget_value(*m_ui->sb_fmax,
                      ::frequency_get(*engine, device::FrequencyType::Fmax));
  modify_widget_value(*m_ui->sb_dFmax,
                      ::frequency_get(*engine, device::FrequencyType::dFmax));
  modify_widget_value(*m_ui->sb_maxSpeed, engine->maxSpeed().value_of());
  modify_widget_value(*m_ui->sb_repeatability,
                      engine->repeatability().value_of());
  modify_widget_value(*m_ui->sb_homePosition,
                      engine->homePosition().value_of());

  //  m_ui->sb_dFmax->setValue(engine->dfmax().value_of());
  //  m_ui->sb_f0->setValue(engine->f0().value_of());
  //  m_ui->sb_fmax->setValue(engine->fmax().value_of());
  //  m_ui->sb_maxSpeed->setValue(engine->maxSpeed().value_of());
  //  m_ui->sb_repeatability->setValue(engine->repeatability());
  //  m_ui->sb_homePosition->setValue(engine->homePosition().value_of());

  //  QLOG_DEBUG() << m_ui->cb_microstep->model();
  modify_widget_value(*m_ui->cb_microstep,
                      QVariant::fromValue(engine->microstep()));
  modify_widget_value(*m_ui->cb_currentAccel,
                      QVariant::fromValue(engine->currentAccel()));
  modify_widget_value(*m_ui->cb_currentStdby,
                      QVariant::fromValue(engine->currentStdby()));

  //  m_ui->cb_microstep->setCurrentIndex(
  //    m_ui->cb_microstep->findData(static_cast<int>(engine->microstep())));
  //  m_ui->cb_currentStdby->setCurrentIndex(m_ui->cb_currentStdby->findData(
  //    static_cast<int>(engine->currentStdby())));
  //  m_ui->cb_currentAccel->setCurrentIndex(m_ui->cb_currentAccel->findData(
  //    static_cast<int>(engine->currentAccel())));
}

void
EngineControl::mixTimerRoutine()
{
  if (--m_mixTime == std::chrono::seconds{ 0 }) {
    QLOG_INFO() << m_mixTimer.get() << "stop";
    m_mixTimer->stop();
  }

  QTime time{ 0, 0, 0, 0 };
  time = time.addSecs(m_mixTime.count());

  m_ui->lcdNumber->display(time.toString("mm:ss"));
}

bool
EngineControl::eventFilter(QObject* watched, QEvent* event)
{
  auto engine_option = currentEngine();
  if (!engine_option.has_value()) {
    return false;
  }
  auto* engine = engine_option.value();

  if (watched == m_ui->lcd_curentPosition) {
    switch (event->type()) {
      case QEvent::Enter:
        QApplication::setOverrideCursor(Qt::PointingHandCursor);
        return true;
      case QEvent::Leave:
        QApplication::restoreOverrideCursor();
        return true;
      case QEvent::MouseButtonPress:
        if (!engine->status().undefinedPos) {
          m_ui->lcd_curentPosition->display("--");
        }
        m_executor.get()
          .execute<cmd::motherboard::EngineGetPositionCurrentOperation>(engine);
        return true;
      default:
        return false;
    }
  }

  if (watched == m_ui->lcd_currentCycle) {
    switch (event->type()) {
      case QEvent::Enter:
        QApplication::setOverrideCursor(Qt::PointingHandCursor);
        return true;
      case QEvent::Leave:
        QApplication::restoreOverrideCursor();
        return true;
      case QEvent::MouseButtonPress:
        m_executor.get()
          .execute<cmd::motherboard::EngineGetMixModeStatusOperation>(engine);
        return true;
      default:
        return false;
    }
  }

  return QWidget::eventFilter(watched, event);
}

} // namespace screen
