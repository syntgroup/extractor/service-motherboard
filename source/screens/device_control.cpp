#include "device_control.h"
#include "ui_device_control.h"

#include "motherboard/cmd/device_get_fw_operation.h"
#include "motherboard/cmd/device_get_id_operation.h"
#include "motherboard/cmd/device_write_id_operation.h"
#include "motherboard/cmd/status_ping_operation.h"
#include "motherboard/cmd/system_get_error_operation.h"
#include "motherboard/cmd/system_soft_reboot_operation.h"
#include "motherboard/cmd/system_zeroize_error_operation.h"
#include "motherboard/device/motherboard_params.h"
#include "transfer/executor.h"
#include <QAction>
#include <QIcon>
#include <QUuid>
#include <QsLog/QsLog.h>

namespace screen {

using namespace cmd::motherboard;

DeviceControl::DeviceControl(transfer::Executor& executor, QWidget* parent)
  : QWidget{ parent }
  , m_executor{ executor }
  , m_ui{ std::make_unique<Ui::DeviceControl>() }
  , m_parameters{ std::make_shared<device::MotherboardParameters>() }
{
  m_ui->setupUi(this);
  m_ui->b_hardReset->setEnabled(false);

  setupConnections();
}

DeviceControl::~DeviceControl() = default;

void
DeviceControl::setupConnections()
{
  connect(m_ui->b_getError, &QPushButton::clicked, this, [this] {
    m_executor.get().execute<SystemGetErrorOperation>(static_cast<uint8_t>(0));
  });
  connect(m_ui->b_resetError, &QPushButton::clicked, this, [this] {
    m_executor.get().execute<SystemZeroizeErrorOperation>();
  });
  connect(m_ui->b_hardReset, &QPushButton::clicked, this, [this] {});
  connect(m_ui->b_softReset, &QPushButton::clicked, this, [this] {
    m_executor.get().execute<SystemSoftRebootOperation>();
  });

  connect(m_ui->b_ping, &QPushButton::clicked, this, [this] {
    m_executor.get().execute<StatusPingOperation>();
  });

  //    connect(motherboard()->getMotherboardParameters(),
  //            &device::MotherboardParameters::,
  //            this,
  //            [this](auto value) {
  //              m_ui->deviceIdLineEdit->setText(QString::number(value));
  //            });

  connect(m_ui->deviceIdButtonPush, &QPushButton::clicked, this, [&] {
    auto allow{ !m_ui->deviceIdLineEdit->text().isEmpty() };
    if (!allow) {
      return;
    }
    m_executor.get().execute<DeviceWriteIdOperation>(
      m_parameters, m_ui->deviceIdLineEdit->text().toUInt());
  });

  //  connect(motherboard()->getMotherboardParameters().get(),
  //          &device::MotherboardParameters)

  //  connect(m_motherboard->motherboardParameters()
  //          &device::MotherboardParams::fwRevChanged,
  //          this,
  //          [this](auto value) {
  //            m_ui->firmwareRevisionLineEdit->setText(QString::number(value));
  //          });

  connect(m_ui->deviceIdButtonGenerate, &QPushButton::clicked, this, [this] {
    auto uuid{ QUuid::createUuid() };
    QLOG_DEBUG() << this << uuid;
    m_ui->deviceIdLineEdit->setText(QString::number(uuid.data1));
  });

  auto* refresh_fw_rev{ m_ui->firmwareRevisionLineEdit->addAction(
    QIcon::fromTheme("view-refresh-symbolic"), QLineEdit::LeadingPosition) };
  connect(refresh_fw_rev, &QAction::triggered, this, [this] {
    m_executor.get().execute<DeviceGetFWOperation>(m_parameters);
  });

  auto* refresh_id{ m_ui->deviceIdLineEdit->addAction(
    QIcon::fromTheme("view-refresh-symbolic"), QLineEdit::LeadingPosition) };
  connect(refresh_id, &QAction::triggered, this, [this] {
    m_executor.get().execute<DeviceGetIdOperation>(m_parameters);
  });
}

} // namespace screen
