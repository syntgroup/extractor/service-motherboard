#include "srvint_executor.h"

#include <QsLog/QsLog.h>

#include "cmd/abstract_serial_operation.h"
#include "transfer/session_srvint.h"

namespace transfer {

SrvIntExecutor::SrvIntExecutor(QObject* parent)
    : AbstractExecutor {parent}
{
}

void SrvIntExecutor::setSession(SrvIntSession* session)
{
  m_session = session;
  m_state = State::online;
}

void SrvIntExecutor::sendToSession(cmd::AbstractSerialOperation* operation)
{
  if (m_state == State::offline) {
    return;
  }

  QLOG_TRACE() << "EXECUTOR" << this << "send to session" << operation;
  Q_ASSERT(operation);

  if (operation->operationType()
      != cmd::AbstractOperation::OperationType::Motherboard)
  {
    return;
  }

  m_session->enqueueOperation(operation);
}

} // namespace transfer
