#include "srvintsession.h"

#include "motherboard/protocol/motherboard_decoder.h"
#include "motherboard/protocol/motherboard_encoder.h"
#include "serial/cmd/abstract_serial_operation.h"
#include "serial/protocols/response_interface.h"
#include <QSerialPort>

namespace {
constexpr std::chrono::milliseconds process_delay{ 10 };
constexpr protocol::PacketID broadcast_id{ 0 };
} // namespace

namespace transfer {

SrvIntSession::SrvIntSession(QObject* parent)
  : AbstractSession{ parent }
  , m_decoder{ std::make_unique<protocol::motherboard::MotherboardDecoder>() }
  , m_encoder{ std::make_unique<protocol::motherboard::MotherboardEncoder>() }
  , m_params{ QSerialPort::Baud115200,
              QSerialPort::Data8,
              QSerialPort::NoParity,
              QSerialPort::OneStop,
              QSerialPort::NoFlowControl }
{
}

SrvIntSession::~SrvIntSession() = default;

auto
SrvIntSession::encoder() const -> Accessor<protocol::EncoderInterface>
{
  return m_encoder;
}

auto
SrvIntSession::decoder() const -> Accessor<protocol::DecoderInterface>
{
  return m_decoder;
}

void
SrvIntSession::processResponse(Accessor<protocol::ResponseInterface> response)
{
  if (response->id() == currentOperation()->id()) {
    processMatchedResponse(response);
  } else if (response->id() == broadcast_id) {
    processBroadcastResponse(response);
  } else {
    processUnmatchedResponse(response);
  }
}

SerialParams
SrvIntSession::serialParams() const
{
  return m_params;
}

int
SrvIntSession::processDelay() const
{
  return process_delay.count();
}

} // namespace transfer
