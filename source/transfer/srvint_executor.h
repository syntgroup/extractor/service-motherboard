#ifndef TRANSFER_SRVINTEXECUTOR_H
#define TRANSFER_SRVINTEXECUTOR_H

#include "di/i_singleton.h"
#include "transfer/abstract_executor.h"

namespace transfer {

class SrvIntSession;

class SrvIntExecutor
    : public transfer::AbstractExecutor
    , public di::ISingleton
{
  Q_OBJECT
  Q_DISABLE_COPY_MOVE(SrvIntExecutor)

  enum class State
  {
    offline,
    online
  };
  State m_state {State::offline};

public:
  explicit SrvIntExecutor(QObject* parent = nullptr);
  ~SrvIntExecutor() override = default;

  void setSession(SrvIntSession* session);

private:
  SrvIntSession* m_session {nullptr};
  void sendToSession(cmd::AbstractSerialOperation* operation) override;
};

} // namespace transfer

#endif  // TRANSFER_SRVINTEXECUTOR_H
