#ifndef TRANSFER_SRVINTSESSION_H
#define TRANSFER_SRVINTSESSION_H

#include "serial/transfer/abstract_session.h"
#include "serial/transfer/serial_params.h"

namespace protocol::motherboard {
class MotherboardEncoder;
class MotherboardDecoder;
} // namespace protocol::motherboard

namespace transfer {

class SrvIntSession : public transfer::AbstractSession
{
  Q_OBJECT
  Q_DISABLE_COPY_MOVE(SrvIntSession)
public:
  explicit SrvIntSession(QObject* parent = nullptr);
  ~SrvIntSession() override;

protected:
  // AbstractSession interface
  [[nodiscard]] auto encoder() const
    -> Accessor<protocol::EncoderInterface> override;
  [[nodiscard]] auto decoder() const
    -> Accessor<protocol::DecoderInterface> override;
  [[nodiscard]] auto serialParams() const -> SerialParams override;
  [[nodiscard]] auto processDelay() const -> int override;
  void processResponse(Accessor<protocol::ResponseInterface> response) override;

private:
  const std::unique_ptr<protocol::DecoderInterface> m_decoder{ nullptr };
  const std::unique_ptr<protocol::EncoderInterface> m_encoder{ nullptr };
  const SerialParams m_params;
};

} // namespace transfer

#endif // TRANSFER_SRVINTSESSION_H
