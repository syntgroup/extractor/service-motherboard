#ifndef DIALOGS_COLOR_SELECT_H
#define DIALOGS_COLOR_SELECT_H

#include <QDialog>

namespace dialogs {

namespace Ui {
class ColorSelect;
}

class ColorSelect : public QDialog
{
  Q_OBJECT

public:
  explicit ColorSelect(QWidget *parent = nullptr);
  ~ColorSelect() override;

  [[nodiscard]] auto result() const { return m_result; }

private:
  //  Ui::ColorSelect *ui;
  QColor m_result;
};


} // namespace dialogs
#endif // DIALOGS_COLOR_SELECT_H
