#include "color_select.h"

#include "motherboard/device/motherboard_values.h"
#include "utils/color_converter.h"
#include <KColorButton>
#include <QColor>
#include <QHBoxLayout>

namespace dialogs {

ColorSelect::ColorSelect(QWidget* parent)
  : QDialog{ parent }
{
  auto* layout{ new QHBoxLayout{ this } };
  setLayout(layout);

  for (auto color = static_cast<int>(device::Color::Black);
       color <= static_cast<int>(device::Color::White);
       ++color) {
    auto qcolor{ utils::ColorConverter::mapDeviceColorToQColor(
      static_cast<device::Color>(color)) };
    auto* button{ new KColorButton{ qcolor, this } };

    disconnect(button, &QPushButton::clicked, nullptr, nullptr);
    connect(button, &QPushButton::clicked, this, [=] {
      m_result = qcolor;
      accept();
    });

    layout->addWidget(button);
  }
}

ColorSelect::~ColorSelect() = default;

} // namespace dialogs
