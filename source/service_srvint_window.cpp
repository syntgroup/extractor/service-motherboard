#include "service_srvint_window.h"
#include "ui_service_srvint_window.h"

#include "screens/buttons_control.h"
#include "screens/connection_control.h"
#include "screens/device_control.h"
#include "screens/engine_control.h"
#include "screens/leds_control.h"
#include "screens/plates_monitor.h"
#include "screens/power_control.h"
#include "screens/voltages_control.h"
#include "transfer/executor.h"
#include "transfer/srvintsession.h"
#include <KAboutApplicationDialog>
#include <KAboutData>
#include <KBusyIndicatorWidget>
#include <KLed>
#include <KPageWidget>
#include <KPageWidgetItem>
#include <QApplication>
#include <QHBoxLayout>
#include <QLabel>
#include <QMessageBox>
#include <QToolBar>

namespace {

template<class T, typename... Args>
[[nodiscard]] auto
prepare_screen(const QString& title, Args&&... args)
{
  gsl::owner<T*> widget{ new T{ std::forward<Args>(args)... } };
  gsl::owner<KPageWidgetItem*> item{ new KPageWidgetItem{ widget, title } };
  item->setHeader(widget->windowTitle());
  item->setIcon(widget->windowIcon());

  return item;
}

} // namespace

ServiceSrvIntWindow::ServiceSrvIntWindow(transfer::SrvIntSession& session,
                                         transfer::Executor& executor,
                                         QWidget* parent)
  : QMainWindow{ parent }
  , m_ui{ std::make_unique<Ui::ServiceSrvIntWindow>() }
  , m_led{ std::make_unique<KLed>() }
  , m_persistentPages{ prepare_screen<screen::ConnectionControl>(
      tr("Connection"),
      session) }
  , m_conditionalPages{
    prepare_screen<screen::DeviceControl>(tr("Device"), executor),
    prepare_screen<screen::EngineControl>(tr("Engines"), executor),
    prepare_screen<screen::PowerControl>(tr("Power"), executor),
    prepare_screen<screen::VoltagesControl>(tr("Voltages"), executor),
    prepare_screen<screen::LedsControl>(tr("LEDs"), executor),
    prepare_screen<screen::ButtonsControl>(tr("Buttons"), executor),
    prepare_screen<screen::PlatesMonitor>(tr("Plates"), executor)
  }
{
  m_ui->setupUi(this);

  std::for_each(m_persistentPages.begin(),
                m_persistentPages.end(),
                [this](auto* page) { m_ui->pageWidget->addPage(page); });

  std::for_each(m_conditionalPages.begin(),
                m_conditionalPages.end(),
                [this](auto* page) { m_ui->pageWidget->addPage(page); });

  switchWidgetsEnabled(true);
  setupConnections();
}

ServiceSrvIntWindow::~ServiceSrvIntWindow() = default;

void
ServiceSrvIntWindow::setupConnections()
{
  connect(m_ui->actionAbout_Qt,
          &QAction::triggered,
          QApplication::instance(),
          &QApplication::aboutQt);

  connect(m_ui->actionAbout, &QAction::triggered, this, [] {
    KAboutApplicationDialog dialog{ KAboutData::applicationData() };
    dialog.exec();
  });

  connect(m_ui->actionQuit,
          &QAction::triggered,
          QApplication::instance(),
          &QApplication::quit);
}

void
ServiceSrvIntWindow::switchWidgetsEnabled(bool state)
{
  std::for_each(m_conditionalPages.begin(),
                m_conditionalPages.end(),
                [state](auto* widget) { widget->setEnabled(state); });

  if (state) {
    m_led->setColor(Qt::green);
    m_led->on();
  } else {
    m_led->setColor(Qt::red);
    m_led->off();
  }
}
