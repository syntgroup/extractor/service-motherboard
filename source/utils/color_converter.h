#ifndef UTILS_COLORCONVERTER_H
#define UTILS_COLORCONVERTER_H

#include "motherboard/device/motherboard_values.h"
#include <QColor>

namespace utils {

struct ColorConverter
{
  static QColor mapDeviceColorToQColor(device::Color color);
  static device::Color mapQColorToDeviceColor(QColor color);
};

} // namespace utils

#endif // UTILS_COLORCONVERTER_H
