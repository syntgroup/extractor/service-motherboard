#include "color_converter.h"

#include "motherboard/device/motherboard_values.h"

namespace utils {

QColor
ColorConverter::mapDeviceColorToQColor(device::Color color)
{
  switch (color) {
    case device::Color::Black:
      return Qt::transparent;
    case device::Color::Blue:
      return Qt::blue;
    case device::Color::Green:
      return Qt::green;
    case device::Color::Cyan:
      return Qt::cyan;
    case device::Color::Red:
      return Qt::red;
    case device::Color::Magenta:
      return Qt::magenta;
    case device::Color::Yellow:
      return Qt::yellow;
    case device::Color::White:
      return Qt::white;
  }
}

device::Color
ColorConverter::mapQColorToDeviceColor(QColor color)
{
  if (color == Qt::blue) {
    return device::Color::Blue;
  }

  if (color == Qt::green) {
    return device::Color::Green;
  }

  if (color == Qt::cyan) {
    return device::Color::Cyan;
  }

  if (color == Qt::red) {
    return device::Color::Red;
  }

  if (color == Qt::magenta) {
    return device::Color::Magenta;
  }

  if (color == Qt::yellow) {
    return device::Color::Yellow;
  }

  if (color == Qt::white) {
    return device::Color::White;
  }

  return device::Color::Black;
}

} // namespace utils
