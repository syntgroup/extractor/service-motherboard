#ifndef MODELS_CURRENTSCALINGMODEL_H
#define MODELS_CURRENTSCALINGMODEL_H

#include "motherboard/device/engine_values.h"
#include <QAbstractListModel>

namespace models {

class CurrentScalingModel : public QAbstractListModel
{
  Q_OBJECT
public:
  explicit CurrentScalingModel(QObject* parent = nullptr);
  ~CurrentScalingModel() override = default;

  [[nodiscard]] int rowCount(const QModelIndex& parent) const override
  {
    return static_cast<int>(m_data.size());
  }
  [[nodiscard]] QVariant data(const QModelIndex& index,
                              int role) const override;

private:
  using DataItem = std::pair<device::Current, QString>;
  std::vector<DataItem> m_data;
};

} // namespace models

#endif // MODELS_CURRENTSCALINGMODEL_H
