#include "current_scaling_model.h"

namespace models {

CurrentScalingModel::CurrentScalingModel(QObject *parent)
  : QAbstractListModel{parent}
{
  for (auto idx = 0; idx < 32; ++idx) {
    const auto label{ QStringLiteral("%1/32").arg(idx + 1) };
    const auto value{ static_cast<device::Current>(idx) };

    m_data.emplace_back(value, label);
  }
}

QVariant
CurrentScalingModel::data(const QModelIndex& index, int role) const
{
  if (!index.isValid()) {
    return {};
  }

  if (role == Qt::DisplayRole) {
    return m_data.at(index.row()).second;
  }

  if (role == Qt::EditRole) {
    return QVariant::fromValue(m_data.at(index.row()).first);
  }

  if (role == Qt::UserRole) {
    return static_cast<int>(m_data.at(index.row()).first);
  }

  return {};
}

} // namespace models
