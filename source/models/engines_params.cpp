#include "engines_params.h"

#include "motherboard/device/engine.h"
#include "motherboard/device/engines.h"
#include <QDebug>

namespace {

QString
mapRowToIdx(int row)
{
  // опа, костыль на лобном месте
  switch (row) {
    case 0:
      return "X";
    case 1:
      return "A";
    case 2:
      return "Z";
    case 3:
      return "B";
    case 4:
      return "C";
    default:
      return "X";
  }
}

}

namespace models {

EnginesParams::EnginesParams(device::Motherboard* data, QObject* parent)
  : QAbstractTableModel{ parent }
  , m_data{ data }
{
  Q_ASSERT(m_data);
}

QVariant
EnginesParams::headerData(int section,
                          Qt::Orientation orientation,
                          int role) const
{
  if (role != Qt::DisplayRole) {
    return QVariant{};
  }

  switch (orientation) {
    case Qt::Horizontal: {
      switch (static_cast<cols>(section)) {
        case Address:
          return QStringLiteral("Address");
        case Name:
          return QStringLiteral("Name");
        case MaxSpeed:
          return QStringLiteral("Maximum speed");
        case Repeatability:
          return QStringLiteral("Repeatability");
        case Miscostep:
          return QStringLiteral("Microstep");
        case AccelF0:
          return QStringLiteral("F0");
        case AccelFmax:
          return QStringLiteral("Fmax");
        case AcceldFmax:
          return QStringLiteral("dFmax");
        case ZeroSwitch:
          return QStringLiteral("Zero switch");
      }

      break;
    }
    case Qt::Vertical: {
      return QString::number(section + 1);
    }
  }

  return QVariant{};
}

int
EnginesParams::rowCount(const QModelIndex& parent) const
{
  if (parent.isValid()) {
    return 0;
  }

  return m_data->engines->count();
}

int
EnginesParams::columnCount(const QModelIndex& parent) const
{
  if (parent.isValid()) {
    return 0;
  }

  return ZeroSwitch + 1;
}

QVariant
EnginesParams::data(const QModelIndex& index, int role) const
{
  if (!index.isValid())
    return QVariant {};

  const auto* engine{
    m_data->engines->engineByAxis(mapRowToIdx(index.row())).value()
  };
  Q_ASSERT(engine);

  switch (static_cast<Qt::ItemDataRole>(role)) {
    case Qt::DisplayRole: {
      switch (static_cast<cols>(index.column())) {
        case Address:
          return QStringLiteral("0x%1").arg(
            QString::number(engine->address(), 16));
        case Name:
          return engine->name();
        case MaxSpeed:
          return engine->maxSpeed().value_of();
        case Repeatability:
          return engine->repeatability().value_of();
        case Miscostep:
          return static_cast<uint8_t>(engine->microstep());
        case AccelF0:
          return engine->f0().value_of();
        case AccelFmax:
          return engine->fmax().value_of();
        case AcceldFmax:
          return engine->dfmax().value_of();
        case ZeroSwitch:
          return static_cast<uint8_t>(engine->zeroSwitch());
        default:
          break;
      }
      break;
    }
    default:
      break;
  }

  return QVariant{};
}

bool
EnginesParams::setData(const QModelIndex& index,
                       const QVariant& value,
                       int role)
{
  if (data(index, role) != value) {
    // FIXME: Implement me!
    emit dataChanged(index, index, QVector<int>{} << role);
    return true;
  }
  return false;
}

Qt::ItemFlags
EnginesParams::flags(const QModelIndex& index) const
{
  if (!index.isValid())
    return Qt::NoItemFlags;

  Qt::ItemFlags theFlags{ QAbstractTableModel::flags(index) };
  if (index.column() > Name) {
    theFlags |= Qt::ItemIsSelectable | Qt::ItemIsEnabled;
  } else {
    theFlags |= Qt::ItemIsSelectable;
  }

  return theFlags;
}

} // namespace models
