#ifndef MODELS_MICROSTEPMODEL_H
#define MODELS_MICROSTEPMODEL_H

#include "motherboard/device/engine_values.h"
#include <QAbstractListModel>

namespace models {

class MicrostepModel : public QAbstractListModel
{
  Q_OBJECT
public:
  explicit MicrostepModel(QObject* paent = nullptr);
  ~MicrostepModel() override = default;

  [[nodiscard]] int rowCount(const QModelIndex& parent) const override
  {
    return static_cast<int>(m_data.size());
  }
  [[nodiscard]] QVariant data(const QModelIndex& index,
                              int role) const override;

private:
  using DataItem = std::pair<device::Microstep, QString>;
  std::vector<DataItem> m_data;
};
} // namespace models

#endif // MODELS_MICROSTEPMODEL_H
