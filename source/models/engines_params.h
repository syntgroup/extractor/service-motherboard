#ifndef MODELS_ENGINESPARAMS_H
#define MODELS_ENGINESPARAMS_H

#include "motherboard/device/engine_defaults.h"
#include <QAbstractTableModel>
#include <QMap>

namespace device {
class Motherboard;
}

namespace models {

class EnginesParams : public QAbstractTableModel
{
  Q_OBJECT

public:
  explicit EnginesParams(device::Motherboard* data, QObject* parent = nullptr);

  // Header:
  QVariant headerData(int section,
                      Qt::Orientation orientation,
                      int role = Qt::DisplayRole) const override;

  // Basic functionality:
  int rowCount(const QModelIndex& parent = QModelIndex{}) const override;
  int columnCount(const QModelIndex& parent = QModelIndex{}) const override;


  QVariant data(const QModelIndex& index,
                int role = Qt::DisplayRole) const override;

  // Editable:
  bool setData(const QModelIndex& index,
               const QVariant& value,
               int role = Qt::EditRole) override;

  Qt::ItemFlags flags(const QModelIndex& index) const override;

private:
  device::Motherboard* m_data{ nullptr };

  enum cols
  {
    Address = 0,
    Name,
    MaxSpeed,
    Repeatability,
    Miscostep,
    AccelF0,
    AccelFmax,
    AcceldFmax,
    ZeroSwitch
  };
};

} // namespace models

#endif // MODELS_ENGINESPARAMS_H
