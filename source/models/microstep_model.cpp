#include "microstep_model.h"

namespace models {

MicrostepModel::MicrostepModel(QObject* parent)
  : QAbstractListModel{ parent }
  , m_data{ { device::Microstep::Step_1_1, QStringLiteral("1/1") },
            { device::Microstep::Step_1_2, QStringLiteral("1/2") },
            { device::Microstep::Step_1_4, QStringLiteral("1/4") },
            { device::Microstep::Step_1_8, QStringLiteral("1/8") },
            { device::Microstep::Step_1_16, QStringLiteral("1/16") },
            { device::Microstep::Step_1_32, QStringLiteral("1/32") },
            { device::Microstep::Step_1_64, QStringLiteral("1/64") },
            { device::Microstep::Step_1_128, QStringLiteral("1/128") },
            { device::Microstep::Step_1_256, QStringLiteral("1/256") } }
{
}

QVariant
MicrostepModel::data(const QModelIndex& index, int role) const
{
  if (!index.isValid()) {
    return {};
  }

  switch (static_cast<Qt::ItemDataRole>(role)) {
    case Qt::DisplayRole:
      return m_data.at(index.row()).second;
    case Qt::EditRole:
      return QVariant::fromValue(m_data.at(index.row()).first);
    case Qt::UserRole:
      return static_cast<int>(m_data.at(index.row()).first);
    default:
      break;
  }

  return {};
}

} // namespace models
