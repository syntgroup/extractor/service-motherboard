install(
    TARGETS service-motherboard_exe service-motherboard_lib
    ${KDE_INSTALL_TARGETS_DEFAULT_ARGS}
)

if(PROJECT_IS_TOP_LEVEL)
  include(CPack)
endif()
